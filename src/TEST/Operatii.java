package TEST;

import TEST.MonitoredData;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

public class Operatii {


    private static String formatDuration(long duration) {
        long hours = TimeUnit.MILLISECONDS.toHours(duration);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(duration) % 60;
        long seconds = TimeUnit.MILLISECONDS.toSeconds(duration) % 60;
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    public static List<MonitoredData> citire() throws IOException {
        Stream<String> date;
        date = Files.lines(Paths.get("Activities.txt"));
        return date.map(e -> {
            String[] el = e.split("\t\t");
            try {
                MonitoredData monitoredData = new MonitoredData(el[0], el[1], el[2]);
                return monitoredData;
            } catch (ParseException e1) {
                e1.printStackTrace();
            }
           return new MonitoredData();
        }).collect(toList());
    }

    public static int dayStarTimeDistinct(List<MonitoredData> ev) {
        return Integer.parseInt(ev.stream().map(s->s.getStartTimeDay()).distinct().count()+"");
    }

    public static void activityNr(List<MonitoredData> ev) throws IOException {
        FileOutputStream g = new FileOutputStream("task2.txt");
        PrintStream gchar = new PrintStream(g);
        Map<String,Long> act= ev.stream().map(s->s.getActivity()).collect(Collectors.groupingBy(s->s,Collectors.counting()));
        AtomicReference<String> citire= new AtomicReference<>("");
        act.forEach((k,v)-> citire.set(citire.get()+k + " " + v+"\n"));
        gchar.println(citire);
        gchar.close();
    }

    public static void activityNrDay(List<MonitoredData> ev) throws IOException {
        FileOutputStream g = new FileOutputStream("task3.txt");
        PrintStream gchar = new PrintStream(g);
        Map<Integer, Map<String, Long>> act = ev.stream().collect(Collectors.groupingBy(MonitoredData::getStartTimeDay,Collectors.groupingBy(MonitoredData::getActivity,Collectors.counting())));
        AtomicReference<String> citire= new AtomicReference<>("");
        act.forEach((k,v)-> citire.set(citire.get()+"DAY :"+ k + " " + v+"\n"));
        gchar.println(citire);
        gchar.close();
    }

    public static void intervalAct(List<MonitoredData> ev) throws IOException {
        FileOutputStream g = new FileOutputStream("task4.txt");
        PrintStream gchar = new PrintStream(g);
        Map<String,Long> act= ev.stream().collect(Collectors.groupingBy(MonitoredData::getActivity,Collectors.summingLong(MonitoredData::getInterval)));
        Map<String,Long> actF= act.entrySet().stream().filter(s->s.getValue()>36000000).collect(toMap(a->a.getKey(),b->b.getValue()));
        AtomicReference<String> citire= new AtomicReference<>("");
        actF.forEach((k,v)-> citire.set(citire.get()+k + " " + formatDuration(v)+"\n"));
        gchar.println(citire);
        gchar.close();
}

    public static void distinctAct (List<MonitoredData> ev) throws FileNotFoundException {
        FileOutputStream g = new FileOutputStream("task5.txt");
        PrintStream gchar = new PrintStream(g);
        Map<String,Double> act =ev.stream()
           .collect(groupingBy(MonitoredData::getActivity,Collectors.
                collectingAndThen(Collectors.mapping(MonitoredData::getInterval,Collectors.
                        averagingDouble(s->(Double.parseDouble(s+"") < 300000)? 1 : 0)),s->s)));
      List<String> actA = act.entrySet().stream().filter(s-> s.getValue()>=0.9).map(s->s.getKey()).distinct().collect(toList());
      AtomicReference<String> citire= new AtomicReference<>("");
      gchar.println("90%");
      actA.forEach((k)-> citire.set(citire.get()+"activity = "+k+"\n"));
      gchar.println(citire);
      gchar.close();
    }
}

