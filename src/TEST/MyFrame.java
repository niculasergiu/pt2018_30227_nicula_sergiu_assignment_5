package TEST;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;

public class MyFrame extends JFrame {

    JLabel af = new JLabel("Lambda Expressions");

    JButton op1 = new JButton("TASK 0");
    JButton op2 = new JButton("TASK 1");
    JButton op3 = new JButton("TASK 2");
    JButton op4 = new JButton("TASK 3");
    JButton op5 = new JButton("TASK 4");
    JButton op6 = new JButton("TASK 5");

    Operatii op = new Operatii();
    List<MonitoredData> ev=op.citire();

    public MyFrame(String title) throws IOException {
        setTitle(title);
        setSize(1000,600);//dimensiune interfata
        setResizable(false);
        JPanel panel = new JPanel();//fcream un obiect de tip JPanel
        panel.setLayout(null);

        panel.setEnabled(true);          ///sa il putem folosii si vedea
        panel.setVisible(true);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setContentPane(panel);

        af.setBounds(150,100,800,50);

        op1.setBounds(200,200,150,60);
        op2.setBounds(450,200,150,60);
        op3.setBounds(700,200,150,60);

        op4.setBounds(200,350,150,60);
        op5.setBounds(450,350,150,60);
        op6.setBounds(700,350,150,60);


        Font f = new Font("", Font.BOLD, 18);
        Font f1 = new Font("", Font.BOLD, 30);
        af.setHorizontalAlignment(JTextField.CENTER);
        af.setFont(f1);
        op1.setHorizontalAlignment(JTextField.CENTER);
        op1.setFont(f);
        op2.setHorizontalAlignment(JTextField.CENTER);
        op2.setFont(f);
        op3.setHorizontalAlignment(JTextField.CENTER);
        op3.setFont(f);
        op4.setHorizontalAlignment(JTextField.CENTER);
        op4.setFont(f);
        op5.setHorizontalAlignment(JTextField.CENTER);
        op5.setFont(f);
        op6.setHorizontalAlignment(JTextField.CENTER);
        op6.setFont(f);

        panel.add(af);
        panel.add(op1);
        panel.add(op2);
        panel.add(op3);
        panel.add(op4);
        panel.add(op5);
        panel.add(op6);

        op1.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                ev.forEach(System.out::println);
            }
        });

        op2.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                System.out.println("Number day: " +op.dayStarTimeDistinct(ev));

            }
        });

        op3.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                try {
                    op.activityNr(ev);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        op4.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                try {
                    op.activityNrDay(ev);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        op5.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                try {
                    op.intervalAct(ev);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        op6.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                try {
                    op.distinctAct(ev);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

    }

    public static void main(String[] args) throws IOException {
        new MyFrame ("TEMA 5");
    }
}
