package TEST;

import java.text.ParseException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MonitoredData {
    private Date startTime;
    private Date endTime;
    private String activity;
    private long interval;


    SimpleDateFormat conv = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Format conv1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Format conv2 = new SimpleDateFormat("dd");


    public MonitoredData(String startTime, String endTime, String activity) throws ParseException {
        this.startTime = conv.parse(startTime);
        this.endTime = conv.parse(endTime);
        this.activity = activity;
        this.interval= this.endTime.getTime()-this.startTime.getTime();
    }
    public MonitoredData(){};


    public Date getStartTime() {
        return startTime;
    }

    public int getStartTimeDay() {
        return Integer.parseInt(conv2.format(startTime));
    }


    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public long getInterval() {
        return interval;
    }

    public void setInterval(long interval) {
        this.interval = interval;
    }


    @Override
    public String toString() {
        return "MonitoredData -> " +
                "startTime = " +conv1.format(startTime) +
                "   endTime = " +conv1.format(endTime) +
                "   activity = " + activity;
    }
}
